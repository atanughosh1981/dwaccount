/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mck.service.dwaccount.web.rest.vm;
